package no.ntnu.idatg1002.group6.todolist.utilities;

import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoList;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoTask;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ToDoListSorterTest {
    ToDoList toDoList1;
    ToDoList toDoList2;
    ToDoList toDoList3;
    ToDoList toDoList4;
    ToDoListSorter toDoListSorter;
    List<ToDoList> lists = new ArrayList<>();

    @BeforeEach
    public void init(){
        toDoList1 = new ToDoList();
        toDoList2 = new ToDoList();
        toDoList3 = new ToDoList();
        toDoList4 = new ToDoList();

        toDoList1.setName("a");
        toDoList2.setName("c");
        toDoList3.setName("b");
        toDoList4.setName("d");

        toDoListSorter = new ToDoListSorter();
        lists.add(toDoList1);
        lists.add(toDoList2);
        lists.add(toDoList3);
        lists.add(toDoList4);
    }

    @Test
    @DisplayName("Test for a-z sorting")
    void sortListsNormalTest(){
        toDoListSorter.setSortBy("a-z");
        lists = toDoListSorter.sort(lists);
        assert (lists.get(0).getName().equals("a"));
        assert (lists.get(1).getName().equals("b"));
        assert (lists.get(2).getName().equals("c"));
        assert (lists.get(3).getName().equals("d"));
    }

    @Test
    @DisplayName("Test for z-a sorting")
    void sortListsReversedTest(){
        toDoListSorter.setSortBy("z-a");
        lists = toDoListSorter.sort(lists);
        assert (lists.get(0).getName().equals("d"));
        assert (lists.get(1).getName().equals("c"));
        assert (lists.get(2).getName().equals("b"));
        assert (lists.get(3).getName().equals("a"));
    }
}
