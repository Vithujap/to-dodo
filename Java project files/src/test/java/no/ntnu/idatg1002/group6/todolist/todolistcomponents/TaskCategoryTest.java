package no.ntnu.idatg1002.group6.todolist.todolistcomponents;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TaskCategoryTest {
    TaskCategory taskCategory;

    @BeforeEach
    public void init() {
        taskCategory = new TaskCategory("category");
    }

    @Test
    @DisplayName("Test for getting the name for a category.")
    void getNameTest() {
        assert (taskCategory.getName().equals("category"));
    }

    @Test
    @DisplayName("Test for setting a name for category")
    void setNameTest() {
        taskCategory.setName("cat");
        assert (taskCategory.getName().equals("cat"));
    }
}
