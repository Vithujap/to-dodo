package no.ntnu.idatg1002.group6.todolist.todolistcomponents;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ToDoListTest {
    ToDoList toDoList;

    @BeforeEach
    public void init() {
        this.toDoList = new ToDoList("list");
    }

    @Test
    void addToListTest() {
        toDoList.addToList(new ToDoTask());
        assert (toDoList.getToDoTasks().size() == 1);
    }

    @Test
    @DisplayName("Test for removing a task for list")
    void removeFromListTest() {
        ToDoTask l = new ToDoTask();
        toDoList.addToList(l);
        toDoList.removeFromList(l);
        assert (toDoList.getToDoTasks().size() == 0);
    }

    @Test
    @DisplayName("Test for testing getter for ToDoTask list")
    void getToDoTasksTest() {
        toDoList.addToList(new ToDoTask());
        toDoList.addToList(new ToDoTask());
        assert (toDoList.getToDoTasks().size() == 2);
    }
}
