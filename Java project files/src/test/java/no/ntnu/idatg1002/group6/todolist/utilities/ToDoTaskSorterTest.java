package no.ntnu.idatg1002.group6.todolist.utilities;

import no.ntnu.idatg1002.group6.todolist.todolistcomponents.TaskCategory;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoList;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoTask;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ToDoTaskSorterTest {
    ToDoList toDoList = new ToDoList();
    ToDoTaskSorter toDoTaskSorter = new ToDoTaskSorter();
    List<ToDoTask> list = new ArrayList<>();

    @BeforeEach
    public void init() {
        ToDoTask toDoTask1 = new ToDoTask();
        toDoTask1.setTaskTitle("a");
        toDoTask1.setTaskPriority(3);
        toDoTask1.setTaskStatus(1);
        toDoTask1.setTaskCategory(new TaskCategory("b"));
        toDoTask1.setTaskStartDate(LocalDate.of(2021, 03, 10));
        toDoTask1.setTaskDueDate(LocalDate.of(2021, 06, 10));
        toDoList.addToList(toDoTask1);

        ToDoTask toDoTask2 = new ToDoTask();
        toDoTask2.setTaskTitle("d");
        toDoTask2.setTaskPriority(2);
        toDoTask2.setTaskStatus(1);
        toDoTask2.setTaskCategory(new TaskCategory("a"));
        toDoTask2.setTaskStartDate(LocalDate.of(2021, 05, 12));
        toDoTask2.setTaskDueDate(LocalDate.of(2022, 05, 20));
        toDoList.addToList(toDoTask2);

        ToDoTask toDoTask3 = new ToDoTask();
        toDoTask3.setTaskTitle("b");
        toDoTask3.setTaskPriority(5);
        toDoTask3.setTaskStatus(1);
        toDoTask3.setTaskStartDate(LocalDate.of(2021, 03, 11));
        toDoTask3.setTaskDueDate(LocalDate.of(2021, 07, 13));
        toDoList.addToList(toDoTask3);

        ToDoTask toDoTask4 = new ToDoTask();
        toDoTask4.setTaskTitle("c");
        toDoTask4.setTaskStatus(1);
        toDoTask4.setTaskCategory(new TaskCategory("c"));
        toDoTask4.setTaskStartDate(LocalDate.of(2021, 04, 06));
        toDoTask4.setTaskDueDate(LocalDate.of(2021, 05, 16));
        toDoList.addToList(toDoTask4);
    }

    @Test
    @DisplayName("Test for title sort")
    void sortByTitleTest() {
        toDoList.setOrderedBy("TITLE");
        list = toDoTaskSorter.listSorter(toDoList);
        assert (list.get(0).getTaskTitle().equals("a"));
        assert (list.get(1).getTaskTitle().equals("b"));
        assert (list.get(2).getTaskTitle().equals("c"));
        assert (list.get(3).getTaskTitle().equals("d"));
    }

    @Test
    @DisplayName("Test for priority sort")
    void sortByPrioritytest() {
        toDoList.setOrderedBy("PRIORITY");
        list = toDoTaskSorter.listSorter(toDoList);
        assert (list.get(0).getTaskPriority() == 5);
        assert (list.get(1).getTaskPriority() == 3);
        assert (list.get(2).getTaskPriority() == 2);
        assert (list.get(3).getTaskPriority() == null);
    }

    @Test
    @DisplayName("Test for category sort")
    void sortByCategoryTest() {
        toDoList.setOrderedBy("CATEGORY");
        list = toDoTaskSorter.listSorter(toDoList);
        assert (list.get(0).getTaskCategory().equals("a"));
        assert (list.get(1).getTaskCategory().equals("b"));
        assert (list.get(2).getTaskCategory().equals("c"));
        assert (list.get(3).getTaskCategory() == null);

    }

    @Test
    @DisplayName("Test for startdate sort")
    void sortByStartdateTest() {
        toDoList.setOrderedBy("STARTDATE");
        list = toDoTaskSorter.listSorter(toDoList);
        assert (list.get(0).getTaskStartDate().toString().equals("2021-03-10"));
        assert (list.get(1).getTaskStartDate().toString().equals("2021-03-11"));
        assert (list.get(2).getTaskStartDate().toString().equals("2021-04-06"));
        assert (list.get(3).getTaskStartDate().toString().equals("2021-05-12"));

    }

    @Test
    @DisplayName("Test for duedate sort")
    void sortByDuedateTest() {
        toDoList.setOrderedBy("DUEDATE");
        list = toDoTaskSorter.listSorter(toDoList);
        assert (list.get(0).getTaskDueDate().toString().equals("2021-05-16"));
        assert (list.get(1).getTaskDueDate().toString().equals("2021-06-10"));
        assert (list.get(2).getTaskDueDate().toString().equals("2021-07-13"));
        assert (list.get(3).getTaskDueDate().toString().equals("2022-05-20"));
    }
}