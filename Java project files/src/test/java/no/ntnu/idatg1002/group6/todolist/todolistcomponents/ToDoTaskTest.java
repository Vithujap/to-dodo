package no.ntnu.idatg1002.group6.todolist.todolistcomponents;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class ToDoTaskTest {

    ToDoTask toDoTask;

    @BeforeEach
    public void init() {
        this.toDoTask = new ToDoTask();
    }

    @Test
    @DisplayName("Test for set and get method for task title")
    void setGetTaskTitleTest() {
        toDoTask.setTaskTitle("title");
        assert (toDoTask.getTaskTitle().equals("title"));
    }

    @Test
    @DisplayName("Test for set and get method for task description")
    void setGetTaskDescription() {
        toDoTask.setTaskDescription("description");
        assert (toDoTask.getTaskDescription().equals("description"));
    }

    @Test
    @DisplayName("Test for set and get task start date")
    void setGetTaskStartDate() {
        toDoTask.setTaskStartDate(LocalDate.now());
        assert (toDoTask.getTaskStartDate().getDayOfMonth() == LocalDate.now().getDayOfMonth());
    }

    @Test
    @DisplayName("Test for set and get task end date")
    void setGetTaskDueDateTest() {
        toDoTask.setTaskDueDate(LocalDate.now());
        assert (toDoTask.getTaskDueDate().getDayOfMonth() == LocalDate.now().getDayOfMonth());
    }

    @Test
    @DisplayName("Test for get task status")
    void getTaskStatusTest() {
        toDoTask.setTaskStatus(1);
        assert (toDoTask.getTaskStatus() == 1);
    }

    @Test
    @DisplayName("Test for get task status index")
    void getTaskStatusIndexTest() {
        assert (toDoTask.getTaskStatusIndex("New") == 0);
    }

    @Test
    @DisplayName("Test for set task status")
    void setTaskStatusTest() {
        ToDoTask toDoTask = new ToDoTask();

        try {
            toDoTask.setTaskStatus(0);
            assert (true);
        } catch (Exception e) {
            fail("Unable to set status within array size.");
        }
        try {
            toDoTask.setTaskStatus(1);
            assert (true);
        } catch (Exception e) {
            fail("Unable to set status within array size.");
        }

        try {
            toDoTask.setTaskStatus(-1);
            fail("Able to set status to negative value.");
        } catch (Exception e) {
            assert (true);
        }

        try {
            toDoTask.setTaskStatus(100);
            fail("Able to set status to value above the array size.");
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    @DisplayName("Test for set and get method for task priority")
    void setGetTaskPriorityTest() {
        toDoTask.setTaskPriority(1);
        assert (toDoTask.getTaskPriority() == 1);
    }

    @Test
    @DisplayName("Test for set and get task category")
    void setGetTaskCategoryTest() {
        TaskCategory t = new TaskCategory("cat");
        toDoTask.setTaskCategory(t);
        assert (toDoTask.getTaskCategory().equals("cat"));
    }
}