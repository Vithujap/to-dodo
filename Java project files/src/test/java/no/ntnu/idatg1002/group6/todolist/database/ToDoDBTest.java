package no.ntnu.idatg1002.group6.todolist.database;

import no.ntnu.idatg1002.group6.todolist.todolistcomponents.TaskCategory;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoList;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoTask;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ToDoDBTest {

    ToDoDB db;
    ToDoListController dbc;

    @BeforeEach
    void setup() {
        db = new ToDoDB();
        dbc = new ToDoListController();
        dbc.clearDatabase();
    }

    @Test
    @DisplayName("Test verifies that you can add lists to db.")
    void addToDatabaseList() {
        ToDoList toDoList = new ToDoList("TestList");
        db.addToDatabase(toDoList);

        assertEquals(1, db.getListsFromDB().size());

    }

    @Test
    @DisplayName("Test verifies that you can add a category to db.")
    void addToDatabaseCategory() {
        TaskCategory taskCategory = new TaskCategory("Cat1");
        db.addToDatabase(taskCategory);

        assertEquals(1, db.getCategoriesFromDB().size());

    }

    @Test
    @DisplayName("Test verifies that you can remove lists from db.")
    void removeFromDatabase() {
        ToDoList toDoList = new ToDoList("TestList");
        db.addToDatabase(toDoList);

        db.removeListFromDatabase(toDoList);

        assertEquals(0, db.getListsFromDB().size());

    }

    @Test
    @DisplayName("Test verifies that you can update lists to db.")
    void updateInDatabase() {
        ToDoList toDoList = new ToDoList("TestList");
        db.addToDatabase(toDoList);

        assertEquals(0, db.getListsFromDB().get(0).getToDoTasks().size());

        toDoList.addToList(new ToDoTask());
        db.updateListInDatabase(toDoList);

        assertEquals(1, db.getListsFromDB().get(0).getToDoTasks().size());
    }

    /*
    @Test
    @DisplayName("Test verifies that you can update category to db.")
    void updateInDatabaseCategory() {
        TaskCategory taskCategory = new TaskCategory("Cat1");
        db.addToDatabase(taskCategory);

        assertEquals("Cat1", db.getCategoriesFromDB().get(0).getName());

        taskCategory.setName("Cat2");

        db.updateCategoryInDatabase(taskCategory);

        assertEquals("Cat2", db.getCategoriesFromDB().get(0).getName());
    } */

    @Test
    @DisplayName("Test verifies that you can get the lists in db.")
    void getDBEntries() {
        ToDoList toDoList1 = new ToDoList("TestList1");
        db.addToDatabase(toDoList1);

        ToDoList toDoList2 = new ToDoList("TestList2");
        db.addToDatabase(toDoList2);

        assertEquals(2, db.getListsFromDB().size());

    }

}