package no.ntnu.idatg1002.group6.todolist.database;

import no.ntnu.idatg1002.group6.todolist.todolistcomponents.TaskCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ToDoListControllerCategoryTest {

    ToDoListController toDoListController;

    @BeforeEach
    void setup() {
        toDoListController = new ToDoListController();
        toDoListController.clearDatabase();
        toDoListController.addSampleData();
    }

    @Test
    @DisplayName("Test for getter of categories.")
    void getAllCategories() {
        ToDoDB toDoDB = new ToDoDB();
        TaskCategory taskCategory1 = new TaskCategory("Cat1");
        TaskCategory taskCategory2 = new TaskCategory("Cat2");
        TaskCategory taskCategory3 = new TaskCategory("Cat3");
        toDoDB.addToDatabase(taskCategory1);
        toDoDB.addToDatabase(taskCategory2);
        toDoDB.addToDatabase(taskCategory3);

        assertEquals(3, toDoListController.getAllCategories().size());

        toDoDB.removeCategoryFromDatabase(taskCategory1);
        toDoDB.removeCategoryFromDatabase(taskCategory2);
        toDoDB.removeCategoryFromDatabase(taskCategory3);
    }

    @Test
    @DisplayName("Test for adding new task.")
    void addNewCategoryToTask() {
        String taskName = "Cool task";
        try {
            toDoListController.addNewCategoryToTask(
                    toDoListController.getGivenList(0),
                    toDoListController.getGivenList(0).getToDoTasks().get(0),
                    taskName);

            assertEquals(1, toDoListController.getAllCategories().size());
        } catch (Exception e) {
            fail("Unable to add new category.");
        }
    }

    @Test
    @DisplayName("Test for adding new task, with task as null.")
    void addNewCategoryToTaskNull() {
        String taskName = "Cool task";
        try {
            toDoListController.addNewCategoryToTask(
                    toDoListController.getGivenList(0),
                    null,
                    taskName);

            fail("Able to add new category with null value.");
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    @DisplayName("Test for adding new task with list as null.")
    void addNewCategoryToTaskListNull() {
        String taskName = "Cool task";
        try {
            toDoListController.addNewCategoryToTask(
                    null,
                    toDoListController.getGivenList(0).getToDoTasks().get(0),
                    taskName);

            fail("Able to add new category with null value.");
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    @DisplayName("Test for adding new task, with task name null.")
    void addNewCategoryToTaskNameNull() {
        try {
            toDoListController.addNewCategoryToTask(
                    toDoListController.getGivenList(0),
                    toDoListController.getGivenList(0).getToDoTasks().get(0),
                    null);
            fail("Able to add new category with null value.");
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    @DisplayName("Test to set category to task")
    void setCategory() {
        TaskCategory taskCategory = new TaskCategory("Exciting task");
        try {
            toDoListController.setCategory(
                    toDoListController.getGivenList(0),
                    toDoListController.getGivenList(0).getToDoTasks().get(0),
                    taskCategory);

            assertEquals(taskCategory.getName(),
                    toDoListController.getCategory(taskCategory.getName()).getName());
        } catch (Exception e) {
            fail("Not able to set task.");
        }
    }

    @Test
    @DisplayName("Test to set category to task, with TaskCategory null")
    void setCategoryCategoryNull() {
        try {
            toDoListController.setCategory(
                    toDoListController.getGivenList(0),
                    toDoListController.getGivenList(0).getToDoTasks().get(0),
                    null);

            fail("Able to set category with null parameter.");
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    @DisplayName("Test to set category to task, with list null")
    void setCategoryListNull() {
        TaskCategory taskCategory = new TaskCategory("Exciting task");
        try {
            toDoListController.setCategory(
                    null,
                    toDoListController.getGivenList(0).getToDoTasks().get(0),
                    null);

            fail("Able to set category with null parameter.");
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    @DisplayName("Test to set category to task, with task null")
    void setCategoryTaskNull() {
        try {
            toDoListController.setCategory(
                    toDoListController.getGivenList(0),
                    null,
                    null);

            fail("Able to set category with null parameter.");
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    @DisplayName("Test for getter of category.")
    void getCategory() {
        ToDoDB toDoDB = new ToDoDB();
        String catName = "Cat1";
        TaskCategory taskCategory1 = new TaskCategory(catName);

        try {
            toDoDB.addToDatabase(taskCategory1);

            assertEquals(taskCategory1, toDoListController.getCategory(catName));
        } catch (Exception e) {
            fail("Didn't find match or name set to null.");
        } finally {
            toDoDB.removeCategoryFromDatabase(taskCategory1);
        }
    }

    @Test
    @DisplayName("Test for getter of category with null.")
    void getCategoryWithNull() {
        try {
            toDoListController.getCategory(null);
            fail("Able to get category with null.");
        } catch (Exception e) {
            assert (true);
        }
    }
}