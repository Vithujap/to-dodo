package no.ntnu.idatg1002.group6.todolist.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import no.ntnu.idatg1002.group6.todolist.log.Log;


/**
 * Class for base structure GUI, using controller classes
 * @author Sander Osvik Brekke
 * @version 06.04.2021
 */
public class ToDoDoApplication extends Application {

    private static Scene scene;
    private static Stage stage;
    private Log log;
    private Log log2;


    /**
     * Start method creating the first page using an FXML-file
     * @param stage
     * @throws Exception if FXML file is not found
     */
    @Override
    public void start(Stage stage) throws Exception {
        log = new Log("HomeViewControllerLog");
        log2 = new Log("ListViewControllerLog");
        stage.setMinWidth(800);
        stage.setMinHeight(600);
        stage.setTitle("The ToDoDo");
        stage.setResizable(false);
        scene = new Scene(FXMLLoader.load(getClass().getResource("/HomeView.fxml")));
        stage.setScene(scene);
        setStage(stage);
        stage.show();
    }

    /**
     * Returns the current scene
     * @return the current scene
     */
    public static Scene getScene() {
        return scene;
    }

    /**
     * Returns the current stage
     * @return the current stage
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * Sets a given stage for the GUI
     * @param stage
     */
    public static void setStage(Stage stage) {
        ToDoDoApplication.stage = stage;
    }


    public static void main(String[] args) { launch(); }
}
