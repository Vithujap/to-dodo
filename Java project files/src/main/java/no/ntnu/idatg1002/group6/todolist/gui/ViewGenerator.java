package no.ntnu.idatg1002.group6.todolist.gui;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;


import java.util.Optional;


/** Class for generating GUI-components common for the two pages and controllers.
 * @author Sander Osvik Brekke
 * @version 06.04.2021
 */
public class ViewGenerator {


    /**
     * Generates an input box with the question string as argument
     * @param string the question to be asked
     * @return the string from the user
     */
    public static String getDialogInput(String string) {
        TextInputDialog td = new TextInputDialog();
        td.setHeaderText(string);

        Optional<String> result = td.showAndWait();
        if (result.isPresent()) {
            if (!td.getEditor().getText().equals(""))
            return td.getEditor().getText();
            else {
                noNameAlert();
                return "";
            }

        }

        return null;
    }

    /**
     * Generates an alert box with a duplicate rename warning when renaming lists
     */
    public static void nameAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Name already existing");
        alert.setHeaderText(null);
        alert.setContentText("There is already a list with this name. Please choose a different name.");
        alert.showAndWait();
    }

    public static void noNameAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Name cannot be empty");
        alert.setHeaderText(null);
        alert.setContentText("Name cannot be empty");
        alert.showAndWait();
    }

    /**
     * Generates a border pane for generating the lists of tasks and tasklists
     * @return borderpane used for one task or tasklist
     */
    public static BorderPane getBorderPane(double s) {
        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(5, 5, 5, 5));
        pane.setMinWidth(s);
        pane.setMaxHeight(20);
        pane.getStyleClass().add("listPane");
        pane.setMinHeight(50);
        pane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                pane.setStyle("-fx-background-color: gray");
            }
        });
        pane.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                pane.setStyle("-fx-background-color: white");
            }
        });
        return pane;
    }


    /**
     * Generates an image view, used for containing delete and edit-buttons for tasks and task
     *  lists, sized for a button icon
     * @param string image path
     * @return ImageView object with the given imag
     */
    public static ImageView getImageView(String string) {
        ImageView imageView = new ImageView();
        imageView.setImage(new Image(string));
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(20);
        return imageView;
    }

    /**
     * Generates an alert dialog when deleting a list or a task list
     * @return boolean, whether or not to complete the deletion
     */
    public static boolean deleteAlert() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Are you sure?");
        alert.setContentText("Are you sure you want to delete this?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            return true;
        } else {
            return false;
        }
    }


}
