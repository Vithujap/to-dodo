package no.ntnu.idatg1002.group6.todolist.todolistcomponents;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;


/**
 * Java class to represent one single task in the ToDoList.
 */
@Entity
public class ToDoTask {

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String taskTitle;
    @Column
    private String taskDescription;
    @Column
    private LocalDate taskStartDate;
    @Column
    private LocalDate taskDueDate;
    @Column
    private Integer taskStatus;
    @Column
    private Integer taskPriority;

    @ManyToOne(cascade = CascadeType.ALL)
    private TaskCategory taskCategory;

    @ElementCollection
    private static final List<String> statusList = List.of("New", "In progress", "Canceled", "Completed");

    private static final List<Integer> priorityList = List.of(1, 2, 3, 4, 5);

    public static List<String> getStatusList() {
        return statusList;
    }

    public static List<Integer> getPriorityList() {
        return priorityList;
    }

    public void ToDoTask() {
    }

    public String getTaskTitle() {
        return taskTitle;
    }

    public void setTaskTitle(String taskTitle) {
        this.taskTitle = taskTitle;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public LocalDate getTaskStartDate() {
        return taskStartDate;
    }

    public void setTaskStartDate(LocalDate taskStartDate) {
        this.taskStartDate = taskStartDate;
    }

    public LocalDate getTaskDueDate() {
        return taskDueDate;
    }

    public void setTaskDueDate(LocalDate taskDueDate) {
        this.taskDueDate = taskDueDate;
    }

    public Integer getTaskStatus() {
        return taskStatus;
    }

    public static Integer getTaskStatusIndex(String string) {
        int count = 0;
        for (String status : statusList) {
            if (string.equals(status)) {
                return count;
            }
            count = count + 1;
        }
        return null;
    }


    public void setTaskStatus(Integer taskStatus) {
        if(taskStatus > statusList.size() || taskStatus < 0) {
            throw new IllegalArgumentException("Status can only be set with index of a item in the array.");
        }
        this.taskStatus = taskStatus;
    }

    public Integer getTaskPriority() {
        try {
            return this.taskPriority;
        } catch (Exception e) {
            return null;
        }
    }

    public void setTaskPriority(Integer taskPriority) {
        this.taskPriority = taskPriority;
    }

    public String getTaskCategory() {
        try {
            return taskCategory.getName();
        } catch (NullPointerException e) {
            return null;
        }
    }

    public void setTaskCategory(TaskCategory taskCategory) {
        this.taskCategory = taskCategory;
    }

    @Override
    public String toString() {
        return "ToDoTask{" +
                "taskTitle='" + taskTitle + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ToDoTask toDoTask = (ToDoTask) o;

        return id.equals(toDoTask.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
