package no.ntnu.idatg1002.group6.todolist.database;

import no.ntnu.idatg1002.group6.todolist.todolistcomponents.TaskCategory;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoList;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoTask;

import java.time.LocalDate;
import java.util.List;

/**
 * Class that acts as a translation layer between the database and the rest of the program.
 *
 * @author Mathias Westby Skoglund
 * @version 1.1
 */
public class ToDoListController {

    private static ToDoDB db;

    /**
     * Constructor that creates a new instance of the db class.
     */
    public ToDoListController() {
        db = new ToDoDB();
    }

    /**
     * Getter for all the to do lists. Returns a List.
     *
     * @return A list of ToDoLists.
     */
    public List<ToDoList> getAllLists() {
        return db.getListsFromDB();
    }

    /**
     * Getter for all categories in the db.
     *
     * @return List of categories.
     */
    public List<TaskCategory> getAllCategories() {
        return db.getCategoriesFromDB();
    }

    /**
     * Getter for list on given index.
     *
     * @param index Index of wanted list.
     * @return The given list.
     */
    public ToDoList getGivenList(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Index can't be smaller than 0.");
        }
        return db.getListsFromDB().get(index);
    }

    /**
     * Gets the index of a given list.
     *
     * @param toDoList The list you want an index for.
     * @return The index.
     */
    public int getIndexOfList(ToDoList toDoList) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        }

        return db.getListsFromDB().indexOf(toDoList);
    }

    /**
     * Getter for all tasks in a list.
     *
     * @param toDoList The list.
     * @return A list of the tasks in the list.
     */
    public List<ToDoTask> getAllTasksInAList(ToDoList toDoList) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        }

        return db.getListsFromDB().get(this.getIndexOfList(toDoList)).getToDoTasks();
    }

    /**
     * Method that adds a todolist to the database.
     *
     * @param toDoList List to be added.
     */
    public void addNewList(ToDoList toDoList) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        }

        db.addToDatabase(toDoList);
    }

    /**
     * Method that creates a new todolist in the database.
     *
     * @param name Name of list to be added.
     */
    public void addNewList(String name) {
        if (name == null) {
            throw new NullPointerException("name cannot be null.");
        }

        db.addToDatabase(new ToDoList(name));
    }

    /**
     * Remove a list with corresponding tasks from the database.
     *
     * @param toDoList ToDoList to be removed.
     */
    public void removeList(ToDoList toDoList) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        }

        db.removeListFromDatabase(toDoList);
    }

    /**
     * Add task to a given list.
     *
     * @param toDoList The ToDoList to add a task to.
     * @param toDoTask The task to be added.
     */
    public void addTaskToList(ToDoList toDoList, ToDoTask toDoTask) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        } else if (toDoTask == null) {
            throw new NullPointerException("toDoTask cannot be null.");
        }

        toDoList.addToList(toDoTask);
        db.updateListInDatabase(toDoList);
    }

    /**
     * Add empty task to given list.
     *
     * @param toDoList The list to add an empty task to.
     */
    public void addTaskToList(ToDoList toDoList) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        }

        ToDoTask toDoTask = new ToDoTask();
        toDoTask.setTaskTitle("New task");
        toDoTask.setTaskDescription("Description ....");
        toDoTask.setTaskStartDate(LocalDate.now());
        toDoTask.setTaskDueDate(LocalDate.now());
        toDoTask.setTaskStatus(0);

        toDoList.addToList(toDoTask);
        db.updateListInDatabase(toDoList);
    }

    /**
     * Method that removes a task from a given list.
     *
     * @param toDoList The list.
     * @param toDoTask The task to be removed.
     */
    public void removeTaskFromList(ToDoList toDoList, ToDoTask toDoTask) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        } else if (toDoTask == null) {
            throw new NullPointerException("toDoTask cannot be null.");
        }

        toDoList.removeFromList(toDoTask);
        db.updateListInDatabase(toDoList);
    }

    /**
     * Updates an existing list in the database.
     *
     * @param toDoList The list to be updated.
     */
    public void updateList(ToDoList toDoList) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        }

        db.updateListInDatabase(toDoList);
    }

    /**
     * Method that adds a new category to an existing task.
     *
     * @param toDoList     List where the task is present.
     * @param toDoTask     The task that needs a new category.
     * @param categoryName The name of the new category.
     */
    public void addNewCategoryToTask(ToDoList toDoList, ToDoTask toDoTask, String categoryName) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        } else if (toDoTask == null) {
            throw new NullPointerException("toDoTask cannot be null.");
        } else if (categoryName == null) {
            throw new NullPointerException("categoryName cannot be null.");
        }

        TaskCategory taskCategory = new TaskCategory(categoryName);

        toDoList.getToDoTasks().stream()
                .filter(task -> (task.equals(toDoTask)))
                .findFirst()
                .get()
                .setTaskCategory(taskCategory);

        this.updateList(toDoList);
    }

    /**
     * Method that sets a category to a task in a list.
     *
     * @param toDoList     The list where the task is.
     * @param toDoTask     The task to set category.
     * @param taskCategory The category to be set.
     */
    public void setCategory(ToDoList toDoList, ToDoTask toDoTask, TaskCategory taskCategory) {
        if (toDoList == null) {
            throw new NullPointerException("toDoList cannot be null.");
        } else if (toDoTask == null) {
            throw new NullPointerException("toDoTask cannot be null.");
        } else if (taskCategory == null) {
            throw new NullPointerException("taskCategory cannot be null.");
        }

        toDoTask.setTaskCategory(taskCategory);

        toDoList.getToDoTasks().stream()
                .filter(task -> (task.equals(toDoTask)))
                .findFirst()
                .get()
                .setTaskCategory(taskCategory);

        this.updateList(toDoList);
    }

    /**
     * Getter for a category with a given name.
     *
     * @param name The name of the category.
     * @return Category object.
     */
    public TaskCategory getCategory(String name) {
        if (name == null) {
            throw new NullPointerException("Name cannot be null");
        }

        return db.getCategoriesFromDB().stream()
                .filter(p -> (p.getName().equalsIgnoreCase(name)))
                .findFirst()
                .get();
    }

    /**
     * Method that closes the factory.
     */
    public void closeConnection() {
        db.closeFactory();
    }

    /**
     * Method that clears all entries in the database.
     */
    public void clearDatabase() {
        db.getListsFromDB().forEach(this::removeList);
        db.getCategoriesFromDB().forEach(taskCategory ->
        {db.removeCategoryFromDatabase(taskCategory);});
    }

    /**
     * Method that adds some sample data to the database.
     */
    public void addSampleData() {
        ToDoList toDoList1 = new ToDoList("School");
        this.addNewList(toDoList1);

        ToDoTask toDoTask1 = new ToDoTask();
        toDoTask1.setTaskTitle("Do programming home work.");

        ToDoTask toDoTask2 = new ToDoTask();
        toDoTask2.setTaskTitle("Do math home work.");

        ToDoTask toDoTask3 = new ToDoTask();
        toDoTask3.setTaskTitle("Do software engineering home work.");

        ToDoList toDoList2 = new ToDoList("Home");
        this.addNewList(toDoList2);

        ToDoTask toDoTask4 = new ToDoTask();
        toDoTask4.setTaskTitle("Take out the garbage");

        ToDoTask toDoTask5 = new ToDoTask();
        toDoTask5.setTaskTitle("Clean the house");

        ToDoTask toDoTask6 = new ToDoTask();
        toDoTask6.setTaskTitle("Build new stairs");

        ToDoList toDoList3 = new ToDoList("Training");
        this.addNewList(toDoList3);

        ToDoTask toDoTask7 = new ToDoTask();
        toDoTask7.setTaskTitle("Take a 5km run");

        ToDoTask toDoTask8 = new ToDoTask();
        toDoTask8.setTaskTitle("Do push ups");

        ToDoTask toDoTask9 = new ToDoTask();
        toDoTask9.setTaskTitle("Do a hangup");

        toDoList1.addToList(toDoTask1);
        toDoList1.addToList(toDoTask2);
        toDoList1.addToList(toDoTask3);
        toDoList2.addToList(toDoTask4);
        toDoList2.addToList(toDoTask5);
        toDoList2.addToList(toDoTask6);
        toDoList3.addToList(toDoTask7);
        toDoList3.addToList(toDoTask8);
        toDoList3.addToList(toDoTask9);

        for (ToDoTask toDoTask : toDoList1.getToDoTasks()) {
            toDoTask.setTaskDescription("Description ....");
            toDoTask.setTaskStartDate(LocalDate.now());
            toDoTask.setTaskDueDate(LocalDate.now());
            toDoTask.setTaskStatus(0);
        }
        for (ToDoTask toDoTask : toDoList2.getToDoTasks()) {
            toDoTask.setTaskDescription("Description ....");
            toDoTask.setTaskStartDate(LocalDate.now());
            toDoTask.setTaskDueDate(LocalDate.now());
            toDoTask.setTaskStatus(0);
        }
        for (ToDoTask toDoTask : toDoList3.getToDoTasks()) {
            toDoTask.setTaskDescription("Description ....");
            toDoTask.setTaskStartDate(LocalDate.now());
            toDoTask.setTaskDueDate(LocalDate.now());
            toDoTask.setTaskStatus(0);
        }

        this.updateList(toDoList1);
        this.updateList(toDoList2);
        this.updateList(toDoList3);

    }

}
