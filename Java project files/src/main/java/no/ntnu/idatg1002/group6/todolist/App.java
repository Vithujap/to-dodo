package no.ntnu.idatg1002.group6.todolist;

import no.ntnu.idatg1002.group6.todolist.database.ToDoListController;
import no.ntnu.idatg1002.group6.todolist.gui.ToDoDoApplication;


public class App {

    public static void main(String[] args) {
        ToDoListController toDoListController = new ToDoListController();

        try{
            if (args[0].equals("clear") || args[1].equals("clear")) {
                toDoListController.clearDatabase();
            }
        } catch (Exception e) {
        }

        try{
            if (args[0].equals("sample")) {
                toDoListController.addSampleData();
            }
        } catch (Exception e) {
        }

        ToDoDoApplication.main(args);

        //Always the last method to be called.
        toDoListController.closeConnection();
    }
}
