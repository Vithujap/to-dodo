package no.ntnu.idatg1002.group6.todolist.database;

import no.ntnu.idatg1002.group6.todolist.todolistcomponents.TaskCategory;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

/**
 * This class interacts with the database. This class is protected, and therefore needs to
 * be accessed through the ToDoListController class.
 *
 * @author Mathias Westby Skoglund
 * @version 1.0
 */
public class ToDoDB {

    //The name of the db
    private static final String PERSISTENCE_UNIT_NAME = "todoDB";
    private final EntityManagerFactory entityManagerFactory;

    /**
     * Constructor that creates a new EntityManagerFactory.
     */
    protected ToDoDB() {
        entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    /**
     * Adds object to the database.
     *
     * @param toDoList Object to be added.
     */
    protected void addToDatabase(ToDoList toDoList) {
        EntityManager em = getEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(toDoList);
            em.getTransaction().commit();
        } finally {
            closeEntityManager(em);
        }
    }

    /**
     * Adds object to the database.
     *
     * @param taskCategory Object to be added.
     */
    protected void addToDatabase(TaskCategory taskCategory) {
        EntityManager em = getEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(taskCategory);
            em.getTransaction().commit();
        } finally {
            closeEntityManager(em);
        }
    }

    /**
     * Removes list from the database.
     *
     * @param toDoList Object to be removed.
     */
    protected void removeListFromDatabase(ToDoList toDoList) {
        EntityManager em = getEntityManager();

        try {
            em.getTransaction().begin();
            if (!em.contains(toDoList)) {
                toDoList = em.merge(toDoList);
            }
            em.remove(toDoList);
            em.getTransaction().commit();
        } finally {
            closeEntityManager(em);
        }
    }

    /**
     * Removes list from the database.
     * TODO Obsolete?
     *
     * @param taskCategory Object to be removed.
     */
    protected void removeCategoryFromDatabase(TaskCategory taskCategory) {
        EntityManager em = getEntityManager();

        try {
            em.getTransaction().begin();
            if (!em.contains(taskCategory)) {
                taskCategory = em.merge(taskCategory);
            }
            em.remove(taskCategory);
            em.getTransaction().commit();
        } finally {
            closeEntityManager(em);
        }
    }

    /**
     * Updates object in the database.
     *
     * @param toDoList Object to be updated.
     */
    protected void updateListInDatabase(ToDoList toDoList) {
        EntityManager em = getEntityManager();

        try {
            em.getTransaction().begin();
            em.merge(toDoList);
            em.getTransaction().commit();
        } finally {
            closeEntityManager(em);
        }
    }

    /**
     * Updates category in the database. Obsolete?
     *
     * @param taskCategory Object to be updated.
     */
    protected void updateCategoryInDatabase(TaskCategory taskCategory) {
        EntityManager em = getEntityManager();

        try {
            em.getTransaction().begin();
            em.merge(taskCategory);
            em.getTransaction().commit();
        } finally {
            closeEntityManager(em);
        }
    }

    /**
     * Method that creates a list with all list entries from the database.
     *
     * @return A list containing the database elements.
     */
    protected List<ToDoList> getListsFromDB() {
        EntityManager entityManager = getEntityManager();

        Query q = entityManager.createQuery("select t from ToDoList t");

        @SuppressWarnings("unchecked")
        List<ToDoList> toDoLists = q.getResultList();

        return toDoLists;
    }

    /**
     * Method that creates a list with all category entries from the database.
     *
     * @return A list containing the database elements.
     */
    protected List<TaskCategory> getCategoriesFromDB() {
        EntityManager entityManager = getEntityManager();

        Query q = entityManager.createQuery("select t from TaskCategory t");

        @SuppressWarnings("unchecked")
        List<TaskCategory> categories = q.getResultList();

        return categories;
    }

    /**
     * Creates a new EntityManager object.
     *
     * @return new EntityManager object.
     */
    private EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    /**
     * Closes the EntityManager if it exists and its open.
     *
     * @param entityManager EntityManager to close.
     */
    private void closeEntityManager(EntityManager entityManager) {
        if (entityManager != null && entityManager.isOpen()) entityManager.close();
    }

    /**
     * Method that closes the EntityManagerFactory.
     */
    protected void closeFactory() {
        entityManagerFactory.close();
    }

}
