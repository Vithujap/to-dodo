package no.ntnu.idatg1002.group6.todolist.gui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import no.ntnu.idatg1002.group6.todolist.database.ToDoListController;

import no.ntnu.idatg1002.group6.todolist.todolistcomponents.TaskCategory;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoList;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoTask;
import no.ntnu.idatg1002.group6.todolist.utilities.ToDoTaskSorter;


import java.awt.*;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller class for the GUI page containing tasks
 *
 * @author Sander Osvik Brekke
 * @version 06.04.2021
 */
public class ListViewController implements Initializable {


    //Defining fields for the class. @FXML-fields are referenced to objects in the GUI
    private ToDoListController toDoListController = new ToDoListController();
    private ToDoList list = toDoListController.getGivenList(HomeViewController.listIndex);
    private Logger logger = Logger.getLogger("ListViewControllerLog");
    private int id;

    private Integer dateYear;
    private String dateDay;
    private String dateMonth;


    @FXML
    TilePane listPane;

    @FXML
    TextField listName;

    @FXML
    AnchorPane editBox;

    @FXML
    TextField taskName;

    @FXML
    TextArea descriptionBox;

    @FXML
    DatePicker startDate;

    @FXML
    DatePicker endDate;

    @FXML
    SplitMenuButton statusSplit;

    @FXML
    SplitMenuButton categorySplit;

    @FXML
    SplitMenuButton prioritySplit;

    @FXML
    SplitMenuButton sortMenu;

    @FXML
    TextField searchBar;


    /**
     * Updates the text in the title of the task on the left side of the list view
     *
     * @param i      the index of the task in the list
     * @param string the new name from the ChangeListener
     */
    public void updateText(int i, String string) {
        BorderPane pane = (BorderPane) listPane.getChildren().get(i);
        HBox vbox = (HBox) pane.getLeft();
        Text text = (Text) vbox.getChildren().get(0);

        if (string.length() >= 20) {
            text.setText(string.substring(0, Math.min(20, string.length())) + "...");
        } else {
            text.setText(string);
        }
    }

    /**
     * Updates the text in the priority of the task on the left side of the list view
     *
     * @param i      the index of the task in the list
     * @param string the new priority from the ChangeListener
     */
    public void updatePriority(int i, String string) {
        BorderPane pane = (BorderPane) listPane.getChildren().get(i);
        HBox vbox = (HBox) pane.getRight();
        VBox vbox2 = (VBox) vbox.getChildren().get(0);

        Text text = (Text) vbox2.getChildren().get(1);
        text.setText(string);
        logger.log(Level.FINE, "User edited the task priority to " + string);
        createTaskView(null);
    }

    /**
     * Updates the text in the dueDate of the task on the left side of the list view
     *
     * @param i      the index of the task in the list
     * @param string the new due date from the ChangeListener
     */
    public void updateDueDate(int i, String string) {
        BorderPane pane = (BorderPane) listPane.getChildren().get(i);
        HBox vbox = (HBox) pane.getRight();
        VBox vbox2 = (VBox) vbox.getChildren().get(1);

        Text text = (Text) vbox2.getChildren().get(1);
        text.setText(string);
        createTaskView(null);
    }


    /**
     * Defining a changeListener used by the description box in the GUI
     */
    ChangeListener<String> descriptionChangeListener = new ChangeListener<String>() {
        @Override
        public void changed(final ObservableValue<? extends String> observable, final String oldValue,
                            final String newValue) {
            list.getToDoTasks().get(id).setTaskDescription(newValue);
            toDoListController.updateList(list);
            logger.log(Level.FINE, "User edited the task description from " + oldValue + " to " + newValue);
        }
    };

    /**
     * Defining a changeListener used by the search bar in the GUI
     */
    ChangeListener<String> searchChangeListener = new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observableValue, String s, String newString) {
            ArrayList<ToDoTask> tasks = new ArrayList<>();
            int length = newString.length();
            for (ToDoTask task : list.getToDoTasks()) {
                if (task.getTaskTitle().contains(newString)) {
                    tasks.add(task);
                }
            }
            if (newString.length() == 0) {
                createTaskView(null);
            } else {
                createTaskView(tasks);
            }
        }
    };

    /**
     * Defining a changeListener used by the title box in the GUI
     */
    ChangeListener<String> titleChangeListener = new ChangeListener<String>() {
        @Override
        public void changed(final ObservableValue<? extends String> observable, final String oldValue,
                            final String newValue) {
            list.getToDoTasks().get(id).setTaskTitle(newValue);
            toDoListController.updateList(list);
            updateText(id, newValue);
            createTaskView(null);
            logger.log(Level.FINE, "User edited the task title from " + oldValue + " to " + newValue);
        }
    };

    /**
     * Defining a changeListener used by the start date box in the GUI
     */
    ChangeListener<LocalDate> startDateChangeListener = new ChangeListener<LocalDate>() {
        @Override
        public void changed(ObservableValue<? extends LocalDate> observableValue, LocalDate localDate, LocalDate t1) {
            t1 = getLocalDate(t1, startDate, list.getToDoTasks().get(id).getTaskStartDate());

            list.getToDoTasks().get(id).setTaskStartDate(t1);
            toDoListController.updateList(list);
            logger.log(Level.FINE, "User edited the start date from " + localDate + " to " + t1);

            createTaskView(null);
        }
    };

    /**
     * Defining a changeListener used by the end date box in the GUI
     */
    ChangeListener<LocalDate> endDateChangeListener = new ChangeListener<LocalDate>() {
        @Override
        public void changed(ObservableValue<? extends LocalDate> observableValue, LocalDate localDate, LocalDate t1) {
            t1 = getLocalDate(t1, endDate, list.getToDoTasks().get(id).getTaskDueDate());
            list.getToDoTasks().get(id).setTaskDueDate(t1);
            toDoListController.updateList(list);
            createTaskView(null);
            updateDueDate(id, t1.toString());
            logger.log(Level.FINE, "User edited the end date from " + localDate + " to " + t1);

        }
    };

    /**
     * A method that filters and limits the date year to avoid high number years that can cause bugs
     *
     * @param t1        the changeListener for the localdate
     * @param dateField the date field used by the user
     * @param taskDate  the date existing in the database for that task
     * @return date
     */
    private LocalDate getLocalDate(LocalDate t1, DatePicker dateField, LocalDate taskDate) {
        try {
            dateYear = t1.getYear();
            if (dateYear.toString().length() > 4) {
                if (t1.getDayOfMonth() < 10) {
                    dateDay = "0" + t1.getDayOfMonth();
                } else {
                    dateDay = String.valueOf(t1.getDayOfMonth());
                }
                if (t1.getMonthValue() < 10) {
                    dateMonth = "0" + t1.getMonthValue();
                } else {
                    dateMonth = String.valueOf(t1.getMonthValue());
                }
                t1 = LocalDate.parse(taskDate.getYear() + "-" + dateMonth + "-" + dateDay);
                dateField.setValue(t1);
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage());
        }
        return t1;
    }


    /**
     * Creating a new category and automatically adding it to the category list
     *
     * @param task the task where the new category is created
     */
    public void createNewCategory(ToDoTask task) {
        logger.log(Level.FINE, "User clicked the create a new category");
        String categoryName = ViewGenerator.getDialogInput("What is the name of " +
                "the new category?");
        if (categoryName == null) {
            logger.log(Level.FINE, "User pressed cancel");
            return;
        } else if (categoryName.isEmpty()) {
            logger.log(Level.FINE, "Category unable to be created. Reason: no name written for the category");
        } else {
            toDoListController.addNewCategoryToTask(list, task, categoryName);
            generateCategorySplit(id);
            categorySplit.setText(toDoListController.getAllCategories().get(toDoListController
                    .getAllCategories().size() - 1).getName());
            logger.log(Level.FINE, "The following category was created: " + categoryName);
        }
    }

    /**
     * Generates the menu items and the text for the category split menu button in the GUI
     *
     * @param i index of the task in the list
     */
    public void generateCategorySplit(int i) {
        ToDoTask task = list.getToDoTasks().get(i);
        categorySplit.getItems().clear();

        MenuItem menuItem = new MenuItem("New category");
        menuItem.setOnAction((e) -> {
            createNewCategory(task);
        });
        categorySplit.getItems().add(menuItem);

        for (TaskCategory category : toDoListController.getAllCategories()) {
            MenuItem menuItem1 = new MenuItem(category.getName());
            menuItem1.setOnAction((e) -> {
                toDoListController.setCategory(list, task, category);
                categorySplit.setText(category.getName());
            });
            categorySplit.getItems().add(menuItem1);
        }

        if (task.getTaskCategory() != null) {
            categorySplit.setText(task.getTaskCategory());
        } else {
            categorySplit.setText("Category");
        }

        createTaskView(null);
    }

    private void sortList() {
        ToDoTaskSorter sorter = new ToDoTaskSorter();
        list.setToDoTasks(sorter.listSorter(list));
    }

    private void setNewSorting(String newSorting) {
        list.setOrderedBy(newSorting);
        toDoListController.updateList(list);
    }

    public void generateSortBySplit() {
        sortMenu.getItems().clear();

        MenuItem sortByTitle = new MenuItem("Title");
        sortByTitle.setOnAction(e -> {
            this.setNewSorting("TITLE");
            createTaskView(null);
        });

        MenuItem sortByPriority = new MenuItem("Priority");
        sortByPriority.setOnAction(e -> {
            this.setNewSorting("PRIORITY");
            createTaskView(null);
        });

        MenuItem sortByCategory = new MenuItem("Category");
        sortByCategory.setOnAction(e -> {
            this.setNewSorting("CATEGORY");
            createTaskView(null);
        });

        MenuItem sortByStartDate = new MenuItem("Start date");
        sortByStartDate.setOnAction(e -> {
            this.setNewSorting("STARTDATE");
            createTaskView(null);
        });

        MenuItem sortByDueDate = new MenuItem("Due date");
        sortByDueDate.setOnAction(e -> {
            this.setNewSorting("DUEDATE");
            createTaskView(null);
        });

        sortMenu.getItems().add(sortByTitle);
        sortMenu.getItems().add(sortByPriority);
        sortMenu.getItems().add(sortByCategory);
        sortMenu.getItems().add(sortByStartDate);
        sortMenu.getItems().add(sortByDueDate);

    }

    /**
     * Generates the menu items and the text for the status split menu button in the GUI
     *
     * @param i index of the task in the list
     */
    public void generateStatusSplit(int i) {
        ToDoTask task = list.getToDoTasks().get(i);
        statusSplit.getItems().clear();

        for (String string : ToDoTask.getStatusList()) {
            MenuItem item = new MenuItem(string);
            item.setOnAction(e -> {
                statusSplit.setText(string);
                task.setTaskStatus(ToDoTask.getTaskStatusIndex(string));
                toDoListController.updateList(list);
                createTaskView(null);
                generateDetailedView(i);
            });
            statusSplit.getItems().add(item);

        }
        statusSplit.setText(ToDoTask.getStatusList().get(task.getTaskStatus()));

    }

    /**
     * Generates the menu items and the text for the priority split menu button in the GUI
     *
     * @param i index of the task in the list
     */
    public void generatePrioritySplit(int i) {
        ToDoTask task = list.getToDoTasks().get(i);
        prioritySplit.getItems().clear();
        for (Integer priority : ToDoTask.getPriorityList()) {
            MenuItem item = new MenuItem(Integer.toString(priority));
            item.setOnAction(e -> {
                prioritySplit.setText(Integer.toString(priority));
                task.setTaskPriority(priority);
                updatePriority(i, priority.toString());
                toDoListController.updateList(list);
            });
            prioritySplit.getItems().add(item);
        }
        if (task.getTaskPriority() != null) {
            prioritySplit.setText(Integer.toString(task.getTaskPriority()));
        } else {
            prioritySplit.setText("Priority");
        }

    }

    /**
     * Method called by the add button in the program, making a new task within the current list. The task is standard
     *
     * @param event
     */
    @FXML
    public void addClick(ActionEvent event) {
        logger.log(Level.FINE, "User clicked the add task button");
        toDoListController.addTaskToList(list);
        toDoListController.updateList(toDoListController.getGivenList(HomeViewController.listIndex));
        createTaskView(null);
    }

    /**
     * Method called by the back-button in the GUI. Goes back to the HomeView
     *
     * @param event
     */
    @FXML
    public void backClick(ActionEvent event) {
        logger.log(Level.FINE, "User clicked the home button");
        try {
            toDoListController.updateList(list);
            ToDoDoApplication.getScene().setRoot((FXMLLoader.load(getClass().getResource("/HomeView.fxml"))));
            logger.log(Level.FINE, "User was sent to the home page");
        } catch (Exception e) {

            logger.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Method called by the delete-button on each task, deleting the task after double checking with a dialogue box
     *
     * @param event
     */
    @FXML
    public void deleteClick(ActionEvent event) {
        logger.log(Level.FINE, "User clicked the delete task button");
        if (ViewGenerator.deleteAlert()) {
            list = toDoListController.getGivenList(HomeViewController.listIndex);
            logger.log(Level.FINE, "The task with the following task title was deleted: " + list.getToDoTasks().get(id).getTaskTitle());
            toDoListController.removeTaskFromList(list, list.getToDoTasks().get(id));
            toDoListController.updateList(list);
            createTaskView(null);
        } else {
            logger.log(Level.FINE, "User pressed cancel");
        }
    }


    /**
     * Generating the detailed view when selecting a task, also setting the changelisteners
     * for seamless editing of tasks
     *
     * @param i the index of the task in the task list
     */
    public void generateDetailedView(int i) {
        id = i;
        editBox.setDisable(false);
        ToDoTask task = list.getToDoTasks().get(i);
        taskName.setText(task.getTaskTitle());
        taskName.textProperty().removeListener(titleChangeListener);
        taskName.textProperty().addListener(titleChangeListener);
        descriptionBox.setText(task.getTaskDescription());
        descriptionBox.textProperty().removeListener(descriptionChangeListener);
        descriptionBox.textProperty().addListener(descriptionChangeListener);
        startDate.setEditable(false);
        startDate.setValue(task.getTaskStartDate());
        startDate.valueProperty().removeListener(startDateChangeListener);
        startDate.valueProperty().addListener(startDateChangeListener);
        endDate.setValue(task.getTaskDueDate());
        endDate.valueProperty().removeListener(endDateChangeListener);
        endDate.valueProperty().addListener(endDateChangeListener);
        endDate.setEditable(false);

        this.generateCategorySplit(i);
        this.generatePrioritySplit(i);
        this.generateStatusSplit(i);

    }

    /**
     * Generating and setting the list of tasks in the current list view, also used for refreshing the task view after
     * changes requiring a refresh
     */
    public void createTaskView(List<ToDoTask> tasks) {
        logger.log(Level.FINE, "User is in the following list: " + list.getName());
        listPane.getChildren().clear();
        this.generateSortBySplit();

        this.sortList();

        if (tasks == null) {
            tasks = list.getToDoTasks();
        }

        listName.setText(list.getName());
        int i = 0;
        for (ToDoTask task : tasks) {
            BorderPane pane = ViewGenerator.getBorderPane(listPane.getPrefWidth() - 20);
            pane.setId(Integer.toString(i));
            Text titleText = new Text(task.getTaskTitle());
            Text endDateText = new Text(task.getTaskDueDate().toString());
            Text taskPriorityText = new Text(String.valueOf(task.getTaskPriority()));

            CheckBox checkBox = new CheckBox();
            checkBox.setId(Integer.toString(i));
            checkBox.setOnMouseClicked(e -> {
                if (checkBox.isSelected()) {
                    list.getToDoTasks().get(Integer.parseInt(checkBox.getId())).setTaskStatus(3);
                    titleText.setStrikethrough(true);
                    taskPriorityText.setStrikethrough(true);
                    endDateText.setStrikethrough(true);
                    toDoListController.updateList(list);
                    this.createTaskView(null);

                } else {
                    list.getToDoTasks().get(Integer.parseInt(checkBox.getId())).setTaskStatus(1);
                    titleText.setStrikethrough(false);
                    taskPriorityText.setStrikethrough(false);
                    endDateText.setStrikethrough(false);
                    toDoListController.updateList(list);
                    this.createTaskView(null);
                }
            });
            if (list.getToDoTasks().get(Integer.parseInt(checkBox.getId())).getTaskStatus() == 3) {
                checkBox.setSelected(true);
                titleText.setStrikethrough(true);
                taskPriorityText.setStrikethrough(true);
                endDateText.setStrikethrough(true);
            }

            if (taskPriorityText.getText() == "null") {
                taskPriorityText.setText("0");
            }
            if (titleText.getText().length() >= 20) {
                titleText.setText(titleText.getText().substring(0, Math.min(20, titleText.getText().length())) + "...");
            }
            VBox vbox4 = new VBox(5);
            vbox4.setMaxHeight(30);
            vbox4.setAlignment(Pos.CENTER);
            vbox4.getChildren().add(checkBox);

            HBox hbox1 = new HBox(20);
            HBox hbox2 = new HBox(20);
            VBox vbox3 = new VBox(5);
            Text dueText = new Text("Due date:");
            vbox3.getChildren().addAll(dueText, endDateText);

            VBox vbox2 = new VBox(5);
            Text priortyText = new Text("Priority:");

            vbox2.getChildren().addAll(priortyText, taskPriorityText);

            //hbox1.setMinHeight(50);
            hbox1.setAlignment(Pos.CENTER_LEFT);
            vbox2.setAlignment(Pos.CENTER);
            vbox3.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER_RIGHT);
            vbox2.setTranslateX(-55);
            vbox3.setTranslateX(-25);

            priortyText.setStyle("-fx-font-weight: Bold");
            dueText.setStyle("-fx-font-weight: Bold");

            hbox1.setMaxWidth(1);

            hbox1.getChildren().addAll(titleText);
            hbox2.getChildren().addAll(vbox2, vbox3, vbox4);

            pane.setLeft(hbox1);
            pane.setRight(hbox2);

            pane.setOnMouseClicked(e -> {
                generateDetailedView(Integer.parseInt(pane.getId()));
                logger.log(Level.FINE, "User clicked on the following task name: " + list.getToDoTasks()
                        .get(Integer.parseInt(pane.getId())).getTaskTitle());
            });

            listPane.getChildren().add(pane);
            i++;
        }
    }

    /**
     * The first method called when starting the listView, generating the list view
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        searchBar.textProperty().addListener(searchChangeListener);
        editBox.setDisable(true);
        createTaskView(null);

    }
}
