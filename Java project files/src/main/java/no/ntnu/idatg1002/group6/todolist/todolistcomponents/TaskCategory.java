package no.ntnu.idatg1002.group6.todolist.todolistcomponents;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Class that represents user defined categories.
 */
@Entity
public class TaskCategory {

    @Id
    private String name;

    /**
     * Constructor that takes category name as a parameter.
     *
     * @param name The name of the category.
     */
    public TaskCategory(String name) {
        if (name == null) {
            throw new NullPointerException("Name cannot be null");
        }

        this.name = name;
    }

    /**
     * Null parameter constructor.
     */
    public TaskCategory() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category: " + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskCategory that = (TaskCategory) o;

        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
