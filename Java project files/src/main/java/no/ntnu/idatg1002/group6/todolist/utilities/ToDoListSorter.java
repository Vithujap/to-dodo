package no.ntnu.idatg1002.group6.todolist.utilities;

import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoList;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class that sorts the list of lists.
 */
public class ToDoListSorter {

    private String sortBy;
    private static final String NORMAL_SORTING = "a-z";
    private static final String REVERSED_SORTING = "z-a";

    /**
     * Creates an instance of ToDoListSorter
     */
    public ToDoListSorter() {
        sortBy = null;
    }

    /**
     * Method that decides which sorting method is called based on the sortBy field.
     *
     * @param lists The list to be sorted.
     * @return The sorted list.
     * @throws NullPointerException When a value is null.
     */
    public List<ToDoList> sort(List<ToDoList> lists) throws NullPointerException {
        if (sortBy == null) {
            return lists;
        }
        switch (sortBy) {
            case NORMAL_SORTING:
                return this.sortListsNormal(lists);
            case REVERSED_SORTING:
                return this.sortListsReversed(lists);
            default:
                return lists;
        }
    }

    /**
     * Method that sorts a list based on the name a-z.
     *
     * @param lists Given lists.
     * @return New sorted list.
     */
    private List<ToDoList> sortListsNormal(List<ToDoList> lists) {
        return lists.stream()
                .filter(s -> s.getName() != null)
                .sorted(Comparator.comparing(ToDoList::getName))
                .collect(Collectors.toList());

    }

    /**
     * Method that sorts a list based on the name z-a.
     *
     * @param lists Given lists.
     * @return New sorted list.
     */
    private List<ToDoList> sortListsReversed(List<ToDoList> lists) {
        return lists.stream()
                .filter(s -> s.getName() != null)
                .sorted(Comparator.comparing(ToDoList::getName).reversed())
                .collect(Collectors.toList());
    }

    /**
     * Setter for the sortBy field.
     *
     * @param sortBy The new sorting.
     */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }
}
