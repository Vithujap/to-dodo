package no.ntnu.idatg1002.group6.todolist.todolistcomponents;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a ToDoList, and it stores ToDoTasks.
 */
@Entity
public class ToDoList {

    @Id
    @GeneratedValue
    private Integer id;

    private String orderedBy;

    private String name;

    //One list for many tasks relationship in db. Tasks are also depenendt on lists.
    @OneToMany(cascade = CascadeType.ALL)
    private List<ToDoTask> toDoTasks = new ArrayList<>();

    /**
     * Constructor with one parameter.
     *
     * @param name Name of the new list.
     */
    public ToDoList(String name) {
        if (name == null) {
            throw new NullPointerException("Name cannot be null");
        } else {
            this.name = name;
        }

        orderedBy = "";
    }

    /**
     * Empty constructor.
     */
    public ToDoList() {
    }

    /**
     * Method for adding toDoTasks to the ArrayList.
     *
     * @param toDoTask Task to add.
     */
    public void addToList(ToDoTask toDoTask) {
        if (toDoTask == null) {
            throw new NullPointerException("Tasks cannot be null.");
        } else {
            toDoTasks.add(toDoTask);
        }
    }

    /**
     * Method for removing toDoTasks to the ArrayList.
     *
     * @param toDoTask Task to remove.
     */
    public void removeFromList(ToDoTask toDoTask) {
        if (toDoTask == null) {
            throw new NullPointerException("Tasks cannot be null.");
        } else {
            toDoTasks.remove(toDoTask);
        }
    }

    /**
     * Getter for the ArrayList
     *
     * @return The tasks ArrayList
     */
    public List<ToDoTask> getToDoTasks() {
        return toDoTasks;
    }

    /**
     * Setter for the ArrayList.
     *
     * @param toDoTasks The new tasks ArrayList.
     */
    public void setToDoTasks(List<ToDoTask> toDoTasks) {
        this.toDoTasks = toDoTasks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrderedBy() {
        return orderedBy;
    }

    public void setOrderedBy(String orderedBy) {
        this.orderedBy = orderedBy;
    }

    @Override
    public String toString() {
        return "ToDoList{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ToDoList toDoList = (ToDoList) o;

        return name != null ? name.equals(toDoList.name) : toDoList.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
