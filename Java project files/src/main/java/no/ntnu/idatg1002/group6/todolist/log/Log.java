package no.ntnu.idatg1002.group6.todolist.log;
import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


/**
 * Class for creating the user log used for the GUI
 * @author Vithujan
 * @version 28.04.2021
 */
public class Log {

    private Logger logger;
    private FileHandler fileHandler;
    private File file;

    /**
     * Creates an instance of Log
     * @throws IOException if an error occurs when creating or finding the log file
     */
    public Log(String fileName) throws IOException {

        logger = Logger.getLogger(fileName);
        logger.setLevel(Level.ALL);
        file = new File(fileName);
        if  (!file.exists()) {
            file.createNewFile();

        }

        fileHandler = new FileHandler(fileName, true);
        logger.addHandler(fileHandler);
        SimpleFormatter simpleFormatter = new SimpleFormatter();
        fileHandler.setFormatter(simpleFormatter);
    }
}