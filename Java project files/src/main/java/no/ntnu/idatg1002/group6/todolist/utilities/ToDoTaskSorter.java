package no.ntnu.idatg1002.group6.todolist.utilities;

import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoList;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoTask;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ToDoTaskSorter {

    private static final String sortByTitle = "TITLE";
    private static final String sortPriority = "PRIORITY";
    private static final String sortByCategory = "CATEGORY";
    private static final String sortByStartDate = "STARTDATE";
    private static final String sortByDueDate = "DUEDATE";


    /**
     * Creates an instance of ToDoTaskSorter
     */
    public ToDoTaskSorter() {
    }

    public List<ToDoTask> listSorter(ToDoList toDoList) throws NullPointerException{

        switch (toDoList.getOrderedBy()) {
            case sortByTitle:
                return this.sortTasksByTitle(toDoList);
            case sortPriority:
                return this.sortTasksByPriority(toDoList);
            case sortByCategory:
                return this.sortTasksByCategory(toDoList);
            case sortByStartDate:
                return this.sortTasksByStartDate(toDoList);
            case sortByDueDate:
                return this.sortTasksByDueDate(toDoList);
            default:
                return toDoList.getToDoTasks();
        }
    }

    private List<ToDoTask> sortTasksByTitle(ToDoList toDoList) {
        List<ToDoTask> sortedList = toDoList.getToDoTasks().stream()
                .filter(s -> s.getTaskTitle() != null && s.getTaskStatus() != 2 && s.getTaskStatus() != 3)
                .sorted(Comparator.comparing(ToDoTask::getTaskTitle))
                .collect(Collectors.toList());

        sortedList.addAll(filterOutDoneTasks(toDoList));

        return sortedList;
    }

    /**
     * A method that sorts the tasks in a to-do-list by priority
     *
     * @return returns a new list that is sorted by priority
     */
    private List<ToDoTask> sortTasksByPriority(ToDoList toDoList) {
        List<ToDoTask> sortedByTitle = toDoList.getToDoTasks().stream()
                .filter(s -> s.getTaskPriority() == null && s.getTaskStatus() != 2 && s.getTaskStatus() != 3)
                .sorted(Comparator.comparing(ToDoTask::getTaskTitle))
                .collect(Collectors.toList());

        List<ToDoTask> sortedList = toDoList.getToDoTasks().stream()
                .filter(s -> s.getTaskPriority() != null && s.getTaskStatus() != 2 && s.getTaskStatus() != 3)
                .sorted(Comparator.comparing(ToDoTask::getTaskPriority).reversed())
                .collect(Collectors.toList());

        sortedList.addAll(sortedByTitle);
        sortedList.addAll(filterOutDoneTasks(toDoList));

        return sortedList;
    }

    /**
     * A method that sorts the tasks in a to-do-list by category.
     *
     * @return returns a new list that is sorted by priority.
     */
    private List<ToDoTask> sortTasksByCategory(ToDoList toDoList) {
        List<ToDoTask> sortedByTitle = toDoList.getToDoTasks().stream()
                .filter(s -> s.getTaskCategory() == null && s.getTaskStatus() != 2 && s.getTaskStatus() != 3)
                .sorted(Comparator.comparing(ToDoTask::getTaskTitle))
                .collect(Collectors.toList());

        List<ToDoTask> sortedList = toDoList.getToDoTasks().stream()
                .filter(s -> s.getTaskCategory() != null && s.getTaskStatus() != 2 && s.getTaskStatus() != 3)
                .sorted(Comparator.comparing(ToDoTask::getTaskCategory))
                .collect(Collectors.toList());

        sortedList.addAll(sortedByTitle);
        sortedList.addAll(filterOutDoneTasks(toDoList));

        return sortedList;
    }

    /**
     * A method that sorts the tasks in a to-do-list by the start date
     *
     * @return returns a new list that is sorted by the start date
     */
    private List<ToDoTask> sortTasksByStartDate(ToDoList toDoList) {
        List<ToDoTask> sortedByTitle = toDoList.getToDoTasks().stream()
                .filter(s -> s.getTaskStartDate() == null && s.getTaskStatus() != 2 && s.getTaskStatus() != 3)
                .sorted(Comparator.comparing(ToDoTask::getTaskTitle))
                .collect(Collectors.toList());

        List<ToDoTask> sortedList = toDoList.getToDoTasks().stream()
                .filter(s -> s.getTaskStartDate() != null && s.getTaskStatus() != 2 && s.getTaskStatus() != 3)
                .sorted(Comparator.comparing(ToDoTask::getTaskStartDate))
                .collect(Collectors.toList());

        sortedList.addAll(sortedByTitle);
        sortedList.addAll(filterOutDoneTasks(toDoList));

        return sortedList;
    }

    /**
     * A method that sorts the tasks in a to-do-list by the due date
     *
     * @return returns a new list that is sorted by the due date
     */
    private List<ToDoTask> sortTasksByDueDate(ToDoList toDoList) {
        List<ToDoTask> sortedByTitle = toDoList.getToDoTasks().stream()
                .filter(s -> s.getTaskDueDate() == null && s.getTaskStatus() != 2 && s.getTaskStatus() != 3)
                .sorted(Comparator.comparing(ToDoTask::getTaskTitle))
                .collect(Collectors.toList());

        List<ToDoTask> sortedList = toDoList.getToDoTasks().stream()
                .filter(s -> s.getTaskDueDate() != null && s.getTaskStatus() != 2 && s.getTaskStatus() != 3)
                .sorted(Comparator.comparing(ToDoTask::getTaskDueDate))
                .collect(Collectors.toList());

        sortedList.addAll(sortedByTitle);
        sortedList.addAll(filterOutDoneTasks(toDoList));

        return sortedList;
    }

    /**
     * Method that returns a list of all the done and canceled tasks.
     *
     * @param toDoList ToDoList to be sorted.
     * @return The sorted list.
     */
    private List<ToDoTask> filterOutDoneTasks(ToDoList toDoList) {
        return toDoList.getToDoTasks().stream()
                .filter(s -> (s.getTaskStatus() == 2 || s.getTaskStatus() == 3))
                .sorted(Comparator.comparing(ToDoTask::getTaskTitle))
                .collect(Collectors.toList());
    }


}
