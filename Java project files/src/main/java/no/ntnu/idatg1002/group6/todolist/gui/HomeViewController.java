package no.ntnu.idatg1002.group6.todolist.gui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Text;
import no.ntnu.idatg1002.group6.todolist.database.ToDoListController;
import no.ntnu.idatg1002.group6.todolist.log.Log;
import no.ntnu.idatg1002.group6.todolist.todolistcomponents.ToDoList;
import no.ntnu.idatg1002.group6.todolist.utilities.ToDoListSorter;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Controller class for the GUI page containing the lists
 */
public class HomeViewController implements Initializable {

    /**
     * Defining fields for the class. @FXML-fields are referenced to objects in the GUI
     */
    public static int listIndex;
    private ToDoListController toDoListController = new ToDoListController();
    private static List<ToDoList> lists;
    private static ToDoListSorter toDoListSorter;
    Logger logger =Logger.getLogger("HomeViewControllerLog");

    @FXML
    TilePane listPane;

    @FXML
    TextField searchBar;

    @FXML
    SplitMenuButton sortByList;

    /**
     * Defining a changeListener used by the search bar in the GUI
     */
    ChangeListener<String> searchChangeListener = new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observableValue, String s, String newString) {
            ArrayList<ToDoList> lists = new ArrayList<>();
            int length = newString.length();
            for (ToDoList list : toDoListController.getAllLists()) {
                if (list.getName().contains(newString)) {
                    lists.add(list);
                }
            }
            if (newString.length() == 0) {
                createListView();
            } else {
                createListView();
            }
        }
    };


    /**
     * Searchmethod used by ChangeListener in the searchfield. Yet to be implemented
     *
     * @param event
     */
    // TODO: 06/04/2021 Implement method
    @FXML
    public void searchClick(ActionEvent event) {

    }

    /**
     * Sorter-method used to sort tasks after different criterias. Yet to be implemented
     *
     * @param event
     */
    // TODO: 06/04/2021 Implemnt method
    @FXML
    public void sortClick(ActionEvent event) {

    }


    /**
     * Method called when clicking the edit button on a list, allowing the user to change the name of the task
     *
     * @param i the index of the list in the list of lists
     */
    public void editClick(int i) {
        logger.log(Level.FINE, "User clicked the edit list button");

        ToDoList toDoList = toDoListController.getGivenList(i);
        String newName = ViewGenerator.getDialogInput("Edit list name");
        if (newName == null) {
            logger.log(Level.FINE, "User pressed cancel");
            return;
        }
        else if (newName.isEmpty()) {
            logger.log(Level.FINE, "List unable to be edited. Reason: no name written for the ToDoList");
        }
        else {
            toDoList.setName(newName);
            logger.log(Level.FINE, "The list with the name '" + toDoList.getName() + "' was changed to the following name: " + newName);
        }
        toDoListController.updateList(toDoList);
        this.createListView();
    }

    /**
     * Method called
     *
     * @param i the index of the list in the list of lists
     */
    public void deleteClick(int i) {
        logger.log(Level.FINE, "User clicked the delete list button");
        if (ViewGenerator.deleteAlert()) {
            ToDoList toDoList = toDoListController.getGivenList(i);
            logger.log(Level.FINE, "The list with the name '" + toDoList.getName() + "' was deleted");
            toDoListController.removeList(toDoList);
            this.createListView();
        }
        else {
            logger.log(Level.FINE, "User pressed cancel");
        }
    }

    /**
     * Method called when adding a new list, allowing the user to set a name for the list
     *
     * @param event
     */
    @FXML
    public void addClick(ActionEvent event) {
        logger.log(Level.FINE, "User clicked the add list button");
        String newListName = ViewGenerator.getDialogInput("New list name");
        if (newListName == null) {
            logger.log(Level.FINE, "User pressed cancel");
            return;
        }
        else if (newListName.isEmpty()) {
            logger.log(Level.FINE, "List unable to be created. Reason: no name written for the ToDoList");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("No name written");
            alert.setHeaderText(null);
            alert.setContentText("There was no name written for the ToDoList. Please write a name for the ToDoList");
            alert.showAndWait();
        }
        else {
            ToDoList newList = new ToDoList(newListName);
            try {
                toDoListController.addNewList(newList);
                logger.log(Level.FINE, "User created a new list with the name '" + newList.getName() +"'");
            } catch (Exception e) {
                ViewGenerator.nameAlert();
                logger.log(Level.WARNING, e.getMessage());
            }
        }
        this.createListView();
    }


    /**
     * Setting the list of lists on the home page, containing buttons and title for each list, in addition to setting
     * the onclick methods
     */
    @FXML
    public void createListView() {
        listPane.getChildren().clear();
        this.generateSortBySplit();

        lists = toDoListSorter.sort(toDoListController.getAllLists());

        int i = 0;
        for (ToDoList list : this.lists) {
            BorderPane pane = ViewGenerator.getBorderPane(780);
            pane.setId(Integer.toString(i));
            Text text = new Text(list.getName());
            ImageView editImageView = ViewGenerator.getImageView("edit_icon.png");
            ImageView deleteImageView = ViewGenerator.getImageView("delete_icon.png");

            Button editButton = new Button();
            Button deleteButton = new Button();

            editButton.setId(Integer.toString(i));
            deleteButton.setId(Integer.toString(i));

            editButton.setGraphic(editImageView);
            deleteButton.setGraphic(deleteImageView);

            editButton.setOnMouseClicked(e -> {
                editClick(Integer.parseInt(editButton.getId()));
            });
            deleteButton.setOnMouseClicked(e -> {
                deleteClick(Integer.parseInt(deleteButton.getId()));
            });

            HBox vbox = new HBox(5);
            vbox.setMaxHeight(30);
            vbox.getChildren().addAll(editButton, deleteButton);
            HBox vbox1 = new HBox(5);
            vbox1.getChildren().add(text);

            pane.setLeft(vbox1);
            pane.setRight(vbox);


            pane.setOnMouseClicked(e -> {
                try {
                    listIndex = toDoListController.getIndexOfList(list);
                    logger.log(Level.FINE, "User clicked on the list called: " + toDoListController.getGivenList(listIndex).getName());
                    ToDoDoApplication.getScene().setRoot((FXMLLoader.load(getClass()
                            .getResource("/ListView.fxml"))));
                    logger.log(Level.FINE, "User entered the task page of the clicked list");
                } catch (IOException exception) {
                    logger.log(Level.WARNING, exception.getMessage());
                }
            });

            listPane.getChildren().add(pane);
            i++;
        }
    }

    /**
     * Method that creates the sort by drop-down menu
     */
    private void generateSortBySplit() {
        sortByList.getItems().clear();

        MenuItem sortByNameNormal = new MenuItem("a-z");
        sortByNameNormal.setOnAction(e -> {
            toDoListSorter.setSortBy("a-z");
            this.createListView();
        });

        MenuItem sortByNameReversed = new MenuItem("z-a");
        sortByNameReversed.setOnAction(e -> {
            toDoListSorter.setSortBy("z-a");
            this.createListView();
        });

        sortByList.getItems().add(sortByNameNormal);
        sortByList.getItems().add(sortByNameReversed);
    }

    /**
     * The first method called when starting the home view, generating the list of lists
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        searchBar.textProperty().addListener(searchChangeListener);
        lists = toDoListController.getAllLists();

        if(toDoListSorter == null) {
            toDoListSorter = new ToDoListSorter();
        }

        this.createListView();
    }
}
