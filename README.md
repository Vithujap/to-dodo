To install the application put the todolist.jar file in desired folder, and run the application by double clicking the jar, or through the terminal using the command:

java -jar todolist.jar

If this does not work, try using the command, Java -version, to check if you have the required java version to run the application.

For more documentation see the GitLab WIKI.
